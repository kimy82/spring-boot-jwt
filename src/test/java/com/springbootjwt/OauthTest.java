package com.springbootjwt;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.springbootjwt.dto.UserDetails;
import com.springbootjwt.model.Timeline;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class OauthTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void canAccessNotSecuredResourceTest() {
		@SuppressWarnings("unchecked")
		List<String> notSecuredList = this.restTemplate.getForObject("/user/notsecured", List.class);
		assertThat(notSecuredList.size(), is(equalTo(1)));
	}

	@Test
	public void canRetrieveTokenTest() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Basic Y2xpZW50SWQ6c29tZXNlY3JldA==");
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		
		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		map.add("username", "springbootjwt");
		map.add("password", "123456");
		map.add("grant_type", "password");
		
		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>( map, headers);
		TokenResponse tokenResponse = this.restTemplate.postForObject("/oauth/token", request, TokenResponse.class);
		assertThat(tokenResponse.getAccess_token(), is(notNullValue()));
	}
	
	@Test
	public void canNotAccessSecureResourcesTest() {
		HttpEntity<Error> error = this.restTemplate.getForEntity("/user/timelines", Error.class);
		assertThat(error.getBody().getError(), is(equalTo("unauthorized")));
	}

}
