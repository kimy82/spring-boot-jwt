package com.springbootjwt;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import com.springbootjwt.model.User;
import com.springbootjwt.repository.UserRepository;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServer extends AuthorizationServerConfigurerAdapter {

	private static final String		PROP_CLIENTID				= "clientId";
	private static final String		PROP_SECRET					= "somesecret";
	private static final Integer	PROP_TOKEN_VALIDITY_SECONDS	= 86400 * 15;

	@Autowired
	private AuthenticationManager	authenticationManager;

	@Autowired
	private UserRepository			userRepository;

	/**
	 * Defines the strategy used to get tokens. In here the only allowed is password.
	 * When using password as a grant authority, the request to get the token must be:
	 * <ul>
	 * <li>Header: Authorization -> Basic <base 64 encode client and secret></li>
	 * <li>Body: username -> username of existing user</li>
	 * <li>Body: password -> password of user</li>
	 * <li>Body: grant_type -> 'password'</li>
	 * </ul>
	 */
	@Override
	public void configure(ClientDetailsServiceConfigurer clientDetails) throws Exception {
		clientDetails.inMemory().withClient(PROP_CLIENTID).scopes("read", "write")
				.authorizedGrantTypes("password").secret(PROP_SECRET)
				.accessTokenValiditySeconds(PROP_TOKEN_VALIDITY_SECONDS);
	}

	/**
	 * Decides the extra information kept within the token. In this case we store id and email of the user. 
	 * 
	 * @param {@link AuthorizationServerEndpointsConfigurer}
	 */
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer authorizationServerEndpointsConfigurer) throws Exception {
		TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
		tokenEnhancerChain.setTokenEnhancers(
				Arrays.asList(new TokenEnhancer() {
					@Override
					public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
						Map<String, Object> additionalInfo = new HashMap<>();
						User user = AuthorizationServer.this.userRepository.findByUsername(authentication.getName());
						additionalInfo.put("userId", user.getId());
						additionalInfo.put("email", user.getEmail());
						((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
						return accessToken;
					}
				}, accessTokenConverter()));

		authorizationServerEndpointsConfigurer.tokenStore(tokenStore())
				.accessTokenConverter(accessTokenConverter())
				.authenticationManager(authenticationManager).tokenEnhancer(tokenEnhancerChain);
	}

	@Bean
	public TokenStore tokenStore() {
		return new JwtTokenStore(accessTokenConverter());
	}

	/**
	 * We define the strategy to sign tokens. This is a symmetric key and should be the same in the resource server.
	 * TODO: The key should be an Asymmetric KeyPair.
	 * 
	 * @return {@link JwtAccessTokenConverter}
	 */
	@Bean
	public JwtAccessTokenConverter accessTokenConverter() {
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		converter.setSigningKey("123");
		return converter;
	}
}
