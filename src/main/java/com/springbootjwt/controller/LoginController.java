package com.springbootjwt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.springbootjwt.dto.UserDetails;
import com.springbootjwt.model.User;
import com.springbootjwt.repository.UserRepository;
import com.springbootjwt.service.AuthenticationService;

@RepositoryRestController
public class LoginController {


	@Autowired
	private AuthenticationService	authenticationService;
	
	@Autowired
	private UserRepository	userRepo;
	
	@Autowired
	private BCryptPasswordEncoder encoder;

	@RequestMapping(value = "/user/logged", method = RequestMethod.GET)
	@ResponseBody
	public UserDetails isLogged() {
		UserDetails userDetails = new UserDetails();
		userDetails.setUserLogged(this.authenticationService.getLoggedUser());
		return userDetails;
	}
	
	@RequestMapping(value = "/user/register", method = RequestMethod.POST)
	@ResponseBody
	public User register(@RequestBody User user) {
		user.setPassword(this.encoder.encode(user.getPassword()));
		user.setActivated(true);
		return this.userRepo.save(user);
	}
}
