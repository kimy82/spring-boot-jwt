package com.springbootjwt.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.springbootjwt.model.Timeline;
import com.springbootjwt.model.User;
import com.springbootjwt.service.AuthenticationService;
import com.springbootjwt.service.Prunetions;

@RepositoryRestController
public class UserController {

	@Autowired
	private AuthenticationService	authenticationService;

	@RequestMapping(value = "/user/timelines", method = RequestMethod.GET)
	@ResponseBody
	public List<Timeline> getTimelines() {
		User user = this.authenticationService.getLoggedUser();
		return Prunetions.pruneUsersOnTimelines(user.getTimelines());
	}

	@RequestMapping(value = "/user/notsecured", method = RequestMethod.GET)
	@ResponseBody
	public List<String> getAllUsernames() {
		List<String> usernames = new ArrayList<>();
		usernames.add("not secured names");
		return usernames;
	}
}
