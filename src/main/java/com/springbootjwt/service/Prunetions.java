package com.springbootjwt.service;

import java.util.List;

import com.springbootjwt.model.Timeline;

public class Prunetions {

	public static List<Timeline> pruneUsersOnTimelines(List<Timeline> timelines){
		timelines.forEach(timeline -> { pruneUsersOnTimeline(timeline); });
		return timelines;
	}

	public static Timeline pruneUsersOnTimeline(Timeline timeline) {
		timeline.setUsers(null); 
		return timeline;
	}
}
