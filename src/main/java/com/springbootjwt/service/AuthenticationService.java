package com.springbootjwt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.springbootjwt.model.User;
import com.springbootjwt.repository.UserRepository;

@Service
public class AuthenticationService {

	@Autowired
	private UserRepository userRepository;

	/**
	 * Returns a user logged.
	 * 
	 * @return User {@link User}
	 */
	public User getLoggedUser() {
		return getUserInDB(SecurityContextHolder.getContext().getAuthentication());
	}

	private User getUserInDB(Authentication auth) {
		return this.userRepository.findByUsername(auth.getName());
	}
}
