package com.springbootjwt.dto;

import com.springbootjwt.model.User;

public class UserDetails {

	private User userLogged;

	public User getUserLogged() {
		return userLogged;
	}

	public void setUserLogged(User userLogged) {
		this.userLogged = userLogged;
	}
}
