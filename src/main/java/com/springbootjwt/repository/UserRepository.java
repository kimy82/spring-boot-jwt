package com.springbootjwt.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.springbootjwt.model.User;


@RepositoryRestResource(collectionResourceRel = "user", path = "user")
@CrossOrigin("*")
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
	User findByUsername(@Param("username") String username);
}
