pgdata="$PWD/db";
initsql="$PWD/docker-entrypoint-initdb.d";
postgresContainerName="springbootjwt-postgres"

#Stop containers
docker rm -f $postgresContainerName

#run docker file
echo "Starting Postgres DB"
docker run -it --name $postgresContainerName -e "POSTGRES_PASSWORD=springbootjwt" -e "POSTGRES_USER=springbootjwt" -e "POSTGRES_DB=springbootjwt" -e "PGDATA=$pgdata" -v $initsql:/docker-entrypoint-initdb.d -p 5432:5432 postgres:9.4